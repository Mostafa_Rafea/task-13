<?php

// Problem 1

$x = 50;
$y = 30;
$z = ($x - $y) / 10;

echo $z;
echo '<hr>';

// Problem 2

$x = 80;
$y = 30;
$z = ($x + $y) * ($x-$y) / $y;

echo $z;
echo '<hr>';

// Problem 3

$x = 70;
$y = 50;
$z = ($x + 30) / $y;

echo $z;
echo '<hr>';

// Problem 4

$x = 20;
$y = 30;
$z = 50;

$k = $z * ($y / 6 + $x / 5);

echo $k;
echo '<hr>';

// Problem 5

$x = 15;
$y = 5;
$z = 10;

$k = 20 / ($x + $y) + $z / ($x + $y) + $y / ($x + $z);

echo $k;
echo '<hr>';

// Problem 6

$x = 120;
$y = 350;
$z = 760;

$k = ( ($y + $x / $z) + ($z + $x / $y) + ($x + $y / $z) ) / ( $x / $y + $y / $z + $x / $x );

echo $k;
echo '<hr>';